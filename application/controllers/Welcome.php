<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$vova = $this->db->query("SELECT * FROM vova")->result();
				
		$this->twig->display('welcome', array(
			'vova' => $vova
		));
	}
}
